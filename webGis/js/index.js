
// 实例化比例尺控件(ScaleLine)
var  scaleLineControl = new ol.control.ScaleLine({
    // 设置比例尺单位为degrees、imperial、us、nautical或metric
    units:"metric"
});
const map = new ol.Map({
    target: 'map',
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })
    ],
    view: new ol.View({
        center: [113.345396,23.140214],
        zoom: 2,
        minZoom:6,
        maxZoom:12,
        rotation:Math.PI/6,
        controls:ol.control.defaults().extend([scaleLineControl])
    })
});
// 实例化zommslide控件加载到地图容器中
var zoomslider = new ol.control.ZoomSlider();
map.addControl(zoomslider);
// 实例化zoomToExtent控件加载到地图容器中
var zoomToExtent = new ol.control.ZoomToExtent({
    extent:[
        13100000,4290000,
        13200000,5210000
    ]
});
map.addControl(zoomToExtent);

