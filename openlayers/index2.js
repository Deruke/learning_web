import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';


OpenLayers.Layer.QQ = OpenLayers.Class(OpenLayers.Layer.TileCache, {
    sateTiles: !1,
    initialize: function(a, b, c) {
        var d = OpenLayers.Util.extend({
                format: "image/png",
                isBaseLayer: !0
            },
            c);
        OpenLayers.Layer.TileCache.prototype.initialize.apply(this, [a, b, {},
            d]),
            this.extension = this.format.split("/")[1].toLowerCase(),
            this.extension = "jpg" == this.extension ? "jpeg": this.extension,
            this.transitionEffect = "resize",
            this.buffer = 0
    },
    getURL: function(a) {
        var b = this.map.getResolution(),
            c = this.map.getMaxExtent(),
            d = this.tileSize,
            e = this.map.zoom,
            f = Math.round((a.left - c.left) / (b * d.w)),
            g = Math.round((c.top - a.top) / (b * d.h)),
            h = new Array(0, 0, 0, 0, 0, 3, 0, 3, 0, 3, 0, 3, 0, 7, 0, 7, 0, 15, 0, 15, 0, 31, 0, 31, 0, 63, 4, 59, 0, 127, 12, 115, 0, 225, 28, 227, 356, 455, 150, 259, 720, 899, 320, 469, 1440, 1799, 650, 929, 2880, 3589, 1200, 2069, 5760, 7179, 2550, 3709, 11520, 14349, 5100, 7999, 23060, 28689, 10710, 15429, 46120, 57369, 20290, 29849, 89990, 124729, 41430, 60689, 184228, 229827, 84169, 128886),
            i = 4 * e,
            j = h[i++],
            k = h[i++],
            l = h[i++],
            h = h[i],
            m = this.sateTiles ? ".jpg": ".png";
        if (f >= j && k >= f && g >= l && h >= g) {
            g = Math.pow(2, e) - 1 - g;
            var n = 'z=' + e + '&x=' + f + '&y=' + g + '&type=vector&style=0&v=1.1.1'
        }
        var o = this.url;
        return OpenLayers.Util.isArray(o) && n && (o = this.selectUrl(n, o)),
        o + n
    },
    clone: function(a) {
        return null == a && (a = new OpenLayers.Layer.QQ(this.name, this.url, this.options)),
            a = OpenLayers.Layer.TileCache.prototype.clone.apply(this, [a])
    },
    CLASS_NAME: "OpenLayers.Layer.QQ"
});


const map = new Map({
    target: 'map',
    layers: [
        new TileLayer({
            source: new OSM()
        })
    ],
    view: new View({
        center: [0, 0],
        zoom: 0
    })
});