import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import XYZ from 'ol/source/XYZ';

// 创建地图
var openStreetMapLayer = new TileLayer({
     source:new OSM()
 });
// 瓦片地图
var openStreetMapLayer = new TileLayer({
    source:new XYZ({
        url:'http://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    })
});
// 高德地图
var opengaodeMapLayer = new TileLayer({
    source:new XYZ({
        url:'http://webst0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=7&x={x}&y={y}&z={z}'
    })
});
var  map = new Map({
    target:'map',
    layers:[
        // openStreetMapLayer
        opengaodeMapLayer
    ],
    view:new View({
        center:[113.345396,23.140214],
        projection:'EPSG:4326',
        zoom:15
    })
})
