$(function () {
    $.getJSON("data/801stand.json", function (data) {
       console.log(data);
        drawstand(data);

    });
});

function drawstand(respJson) {

    var baseDiv=$("<div lname='"+this.lname+"' class='mainbamboo mainbamboo-selected' name='bamboomap_"+this.lname+"'>" +
        "<div class='numbamboo'>"+this.lname+"</div>" +
        "</div>");

    $("<div class='station_A'>"+respJson.jsonData.endStandName+"<a href='#' class='endStandBus'>0辆</a></div>")
        .appendTo(baseDiv);

    var innerBambooDiv=$("<div class='middle_2' style='float:left;overflow:auto'></div>");
    //add down station

    var downStandStr="<div class='bus_line down'>";

    for(var i=0;i<respJson.jsonData.downStand.length;i++){
        data=respJson.jsonData.downStand[i];
        if(i==0){
            var lsguids =lsguids+','+data.sguid;
            downStandStr+="<div class='station start'  slno='"+(i+1)+"' sguid='"+data.sguid+"'>" +
                "<a href='#' sname='"+data.sname+"' title='"+data.sname+";最新车辆 :"+data.busno+";更新时间:"+data.reportdate+"'><i></i>"+(data.sname.length>4?data.sname.substring(0,4):data.sname)+(i+1)+"</a>" +
                "</div>";
        }else if(i==respJson.jsonData.downStand.length-1){
            var leguids=leguids+','+data.sguid;
            downStandStr+="<div class='station end' slno='"+(i+1)+"' sguid='"+data.sguid+"'><a href='#' " +
                "sname='"+data.sname+"' title='"+data.sname+";最新车辆 :"+data.busno+";更新时间:"+data.reportdate+"'><i></i>"+(data.sname.length>4?data.sname.substring(0,4):data.sname)+(i+1)+"</a></div>";
        }else{
            downStandStr+="<div class='station' slno='"+(i+1)+"' sguid='"+data.sguid+"'><a href='#' " +
                " sname='"+data.sname+"' title='"+data.sname+";最新车辆 :"+data.busno+";更新时间:"+data.reportdate+"'><i></i>"+(data.sname.length>4?data.sname.substring(0,4):data.sname)+(i+1)+"</a></div>";
        }
    }


    downStandStr+="</div>";
    $(downStandStr).appendTo(innerBambooDiv);
    //end add down station
    $("<div class='clear'></div>").appendTo(innerBambooDiv);

    innerBambooDiv.appendTo(baseDiv);


    //add up station
    var data;
    var upStandStr="<div class='bus_line up'>";


    if(respJson.jsonData.upStand){
        for(var i=(respJson.jsonData.upStand.length-1);i>-1 ;i--){
            data=respJson.jsonData.upStand[i];
            if(i==respJson.jsonData.upStand.length-1){
                var lsguids=data.sguid;
                upStandStr+="<div class='station end' slno='"+(i+1)+"' sguid='"+data.sguid+"'><a href='#' " +
                    " sname='"+data.sname+"' title='"+data.sname+";最新车辆 :"+data.busno+";更新时间:"+data.reportdate+"'><i></i>"+(i+1)+(data.sname.length>4?data.sname.substring(0,4):data.sname)+"</a></div>";
            }else if(i==0){
                leguids=data.sguid;
                upStandStr+="<div class='station start' slno='"+(i+1)+"' sguid='"+data.sguid+"'><a href='#' " +
                    " sname='"+data.sname+"' title='"+data.sname+";最新车辆 :"+data.busno+";更新时间:"+data.reportdate+"'><i></i>"+(i+1)+(data.sname.length>4?data.sname.substring(0,4):data.sname)+"</a></div>";
            }else{
                upStandStr+="<div class='station' slno='"+(i+1)+"' sguid='"+data.sguid+"'><a href='#' " +
                    " sname='"+data.sname+"' title='"+data.sname+";最新车辆 :"+data.busno+";更新时间:"+data.reportdate+"'><i></i>"+(i+1)+(data.sname.length>4?data.sname.substring(0,4):data.sname)+"</a></div>";
            }

        }
    }

    upStandStr+="</div>";
    $(upStandStr).appendTo(innerBambooDiv);
    //end add up station
    innerBambooDiv.appendTo(baseDiv);
    $("<div class='station_B'  style='float: right;'> "+respJson.jsonData.startStandName+" <a href='#' class='startStandBus'>0辆</a></div>").appendTo(baseDiv);
    $("<div class='clear'></div>").appendTo(baseDiv);



    baseDiv.prependTo("#dispatchBambooArea");
    resizeAllBamboo();
}

function resizeAllBamboo(){
    $(".middle_2").each(function(){
        var temp = $(this).parent().width()-$(this).parent().find(".station_A").width()-
            $(this).parent().find(".station_B").width()-120;
        $(this).css({"width":temp+"px"})
    });
    $(".down").each(function(){
        var temp = $(this).find(".station").size()*24+$(this).find(".bus").size()*14;
        $(this).css({"width":temp+"px"})
    });
    $(".up").each(function(){
        var temp = $(this).find(".station").size()*24+$(this).find(".bus").size()*14;
        $(this).css({"width":temp+"px"})
    });
    //上行 高度自适应 底对齐
    $(".down .station").each(function(){
        var temp = $(this).parents(".down").height()-$(this).height()-12
        $(this).css({"bottom":"-"+temp+"px"})
    });
    $(".down .bus").each(function(){
        var temp = $(this).parents(".down").height()-$(this).height()-12
        $(this).css({"bottom":"-"+temp+"px"})
    });
    //
    $(".station_A").each(function(){
        var temp = $(this).parent().find(".up").height()-$(this).height()
        $(this).css({"bottom":"-"+temp+"px"})
    });
    $(".station_B").each(function(){
        var temp = $(this).parent().find(".up").height()-$(this).height()
        $(this).css({"bottom":"-"+temp+"px"})
    });
}