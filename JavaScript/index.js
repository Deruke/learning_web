var o1 = {};
var o2 =new Object();
var o3 = new f1();

function f1(){};
var f2 = function(){};
var f3 = new Function('str','console.log(str)');

console.log(typeof Object); //function
console.log(typeof Function); //function

console.log(typeof f1); //function
console.log(typeof f2); //function
console.log(typeof f3); //function

console.log(typeof o1); //object
console.log(typeof o2); //object
console.log(typeof o3); //object



function Person(name, age, job) {
    this.name = name;
    this.age = age;
    this.job = job;
    this.sayName = function() { alert(this.name) }
}
var person1 = new Person('Zaxlct', 28, 'Software Engineer');
var person2 = new Person('Mick', 23, 'Doctor');
console.log(person1.constructor == Person); //true
console.log(person2.constructor == Person); //true

// 在 JavaScript 中，每当定义一个对象（函数也是对象）时候，
// 对象中都会包含一些预定义的属性。其中每个函数对象都有一个prototype 属性，
// 这个属性指向函数的原型对象。
function Person() {}
Person.prototype.name = 'Zaxlct';
Person.prototype.age  = 28;
Person.prototype.job  = 'Software Engineer';
Person.prototype.sayName = function() {
    alert(this.name);
}

var person1 = new Person();
person1.sayName(); // 'Zaxlct'

var person2 = new Person();
person2.sayName(); // 'Zaxlct'

console.log(person1.sayName == person2.sayName); //true

// 每个对象都有 __proto__ 属性，但只有函数对象才有 prototype 属性

// 原型对象
Person.prototype = {
    name:  'Zaxlct',
    age: 28,
    job: 'Software Engineer',
    sayName: function() {
        alert(this.name);
    }
}

var A = new Person();
Person.prototype = A;

// 结论：原型对象（Person.prototype）是 构造函数（Person）的一个实例。

function Person(){};
console.log(Person.prototype) //Person{}
console.log(typeof Person.prototype) //Object
console.log(typeof Function.prototype) // Function，这个特殊
console.log(typeof Object.prototype) // Object
console.log(typeof Function.prototype.prototype) //undefined

var A = new Function ();
Function.prototype = A;
// 通过 new Function( ) 产生的对象都是函数对象。
// 因为 A 是函数对象，所以Function.prototype 是函数对象。

// 原型对象的作用
var Person = function(name){
    this.name = name; // tip: 当函数执行时这个 this 指的是谁？
};
Person.prototype.getName = function(){
    return this.name;  // tip: 当函数执行时这个 this 指的是谁？
}
var person1 = new person('Mick');
person1.getName(); //Mick

var person1 = new person('Mick');
person1.name = 'Mick'; // 此时 person1 已经有 name 这个属性了
person1.getName(); //Mick

// 两次 this 在函数执行时都指向 person1。








